var API_KEY = "HOLCAStI6Z0OfdoPbjdSg5b41Q17w2W5P4WuoIBdC66Z54kUEvGWPIe33UYC";
var BASE_URL = "https://soccer.sportmonks.com/api/v2.0/";

var clubs = [];
//*********************************************
//HELPER FUNCTIONS
//*********************************************

// function to get the league standings
function GetLeagueStandings(){
  var standingUrl = BASE_URL + "standings/season/825?api_token=" + API_KEY;
      $.ajax({
        url: standingUrl
      }).then(function(data) {
        
      for( var i=0 ; i < data.data[1].standings.data.length; i++){
        var row = GenerateTableRowForLeagueStandings(data.data[1].standings.data[i]);
        document.getElementById("tableBody").appendChild(row) ;
        
        saveClubInfo(data.data[1].standings.data[i]);
      }
      $('#league-table').DataTable({
        "pageLength": 20
      });

    });
}

// function to get the club info
function GetClubInfo(teamId, clubName){
  var teamsUrl = BASE_URL + "/teams/"+teamId+"?api_token=" + API_KEY +"&include=squad";
      $.ajax({
        url: teamsUrl
      }).then(function(data) {
        clubs[clubName].players = [];
        fillClubModal(data.data);
        fillClubSquad(data.data,clubName);
    });
}

// function to player info
function GetPlayerInfo(playerId, playerIndex, teamName, showModal){
  var playerUrl = BASE_URL + "players/"+playerId+"?api_token=" + API_KEY;
  $.ajax({
    url: playerUrl
  }).then(function(data) {
    clubs[teamName].players[playerIndex].name = data.data.common_name;
    
  });
}

//*********************************************
//HTML RELATED FUNCTIONS
//*********************************************

//This functions generates a row for tthe table League Standings according the given data
function GenerateTableRowForLeagueStandings(entryData){
  var tableRow = document.createElement("tr");
  
  tableRow.appendChild(createCell(entryData.position));
  tableRow.appendChild(createCellWithClass(entryData.team_name,"team-name"));
  tableRow.appendChild(createCell(entryData.overall.games_played));
  tableRow.appendChild(createCell(entryData.overall.won));
  tableRow.appendChild(createCell(entryData.overall.draw));
  tableRow.appendChild(createCell(entryData.overall.lost));
  tableRow.appendChild(createCell(entryData.overall.goals_scored));
  tableRow.appendChild(createCell(entryData.overall.goals_scored - entryData.overall.goals_against ));
  tableRow.appendChild(createCell(entryData.points));
  
  return tableRow;
}

//This functions generates a row for tthe table League Standings according the given data
function GenerateTableRowForPlayers(player){
  var tableRow = document.createElement("tr");
  
  tableRow.appendChild(createCell(player.number));
  tableRow.appendChild(createCell(player.name));
  tableRow.appendChild(createCell(player.position));
  tableRow.appendChild(createCell(player.appearences));
  tableRow.appendChild(createCell(player.lineups));
  tableRow.appendChild(createCell(player.goals));
  tableRow.appendChild(createCell(player.assists));
  
  return tableRow;
}

//fill club name and logo HTML elements
function fillClubModal(data){
  document.getElementById("clubName").innerHTML = data.name;
  document.getElementById("clubLogo").setAttribute("src", data.logo_path);
}

//fill the table related with players in squad
function fillClubSquad(data, clubName){
  document.getElementById("players").innerHTML ="";
  var playerData = data.squad.data;
  clubs[clubName].players = [];
  for(var i =0; i< playerData.length; i++){
    clubs[clubName].players.push(new Player(
      data.squad.data[i].player_id,
      data.squad.data[i].number,
      data.squad.data[i].position_id,
      data.squad.data[i].appearences,
      data.squad.data[i].lineups,
      data.squad.data[i].goals,
      data.squad.data[i].assists     
    ));

    GetPlayerInfo(data.squad.data[i].player_id, i,clubName, (i==playerData.length -1));
    
  }
  
  //THIS IS A FIX
  //To fix the problem of generate only the table when all ajax request was finished i use this methos
    $(document).ajaxStop(function () {
      for(var i =0; i<clubs[clubName].players.length; i++){
        var row = GenerateTableRowForPlayers(clubs[clubName].players[i]);
        document.getElementById("players").appendChild(row) ;
      }
      $('#DescModal').modal("show");
  });
}

//save team id of the club and create players array
function saveClubInfo(entryData){
  clubs[entryData.team_name] = {teamId: entryData.team_id, players : []};
}


//*********************************************
//HELPER FUNCTIONS
//*********************************************

//Create a table cell with the given value
function createCell(value){
  var cell = document.createElement("td");
  cell.innerHTML = value;
  return cell;
}

//Create a table cell with the given value and the given class
function createCellWithClass(value, className){
  var cell = document.createElement("td");
  cell.innerHTML = value;
  cell.classList.add(className);
  return cell;
}

//Class Player
function Player(id,number, position, appearences, lineups, goals, assists) {
    this.id = id;
    this.number = number;
    this.name = "";
    this.position = position;
    this.appearences = appearences;
    this.lineups = lineups;
    this.goals = goals;
    this.assists = assists;
}

//On document ready get the league standing and instantiate listerners for click on club
$(document).ready(function() {
    GetLeagueStandings();
      $('#league-table').on('click', '.team-name', function () {
        var name =  $(this).context.innerHTML;
        GetClubInfo(clubs[name].teamId,name);
    });
} );